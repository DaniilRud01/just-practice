import "app/styles/index.scss";

import type { Preview } from "@storybook/react";
import { routerDecorator, themeDecorator } from "../../src/shared/config/storybook";
import { ThemeVariant } from "../../src/app/enums";

const preview: Preview = {
  decorators: [themeDecorator(ThemeVariant.LIGHT), routerDecorator],
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};

export default preview;
