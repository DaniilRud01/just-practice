import { TsconfigPathsPlugin } from "tsconfig-paths-webpack-plugin"
import { getLocalIdentName } from "css-loader-shorter-classnames"

const getLocalIdent = getLocalIdentName();

import type { StorybookConfig } from "@storybook/react-webpack5";
import { cssLoaderConfig } from "../loaders";
import { RuleSetRule, DefinePlugin } from "webpack";

const config: StorybookConfig = {
  stories: ["../../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    {
      name: "storybook-css-modules",
      options: {
        cssModulesLoaderOptions: {
          importLoaders: 1,
          modules: {
            getLocalIdent,
          },
        },
      }
    },
  ],
  framework: {
    name: "@storybook/react-webpack5",
    options: {},
  },
  docs: {
    autodocs: "tag",
  },
  webpackFinal: async (config) => {
    config.resolve.plugins = [
      ...(config.resolve.plugins || []),
      new TsconfigPathsPlugin({
        extensions: config.resolve.extensions,
      }),
    ];
    config.module.rules = config.module.rules.map((rule: RuleSetRule) => {
      if (/svg/.test(rule.test as string)) {
        return { ...rule, exclude: /\.svg$/i }
      }

      return rule
    });
    config.module.rules.push(cssLoaderConfig(true))
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
    })
    config.plugins.push(new DefinePlugin({ __IS_DEV: true }))
    return config;
  },
};
export default config;
