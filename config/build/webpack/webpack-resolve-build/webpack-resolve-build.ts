import { ResolveOptions } from "webpack";
import { IWebpackConfigType } from "../type/webpack-config.type";

export const webpackResolveBuild = (options: IWebpackConfigType): ResolveOptions => {
    return  {
        extensions: [
            ".tsx", ".ts", ".js", "./src"
        ],
        preferRelative: true,
        alias: {},
        mainFiles: ["index"],
        modules: [options.paths.src, "node_modules"],
    };
};