import { IWebpackConfigType } from "../type/webpack-config.type.js";
import type { Configuration as DevServerConfiguration } from "webpack-dev-server";

export const webpackDevServerBuild  = ({ port } : IWebpackConfigType): DevServerConfiguration => {
    return {
        port,
        open: true,
        historyApiFallback: true
    };
};