import { RuleSetRule } from "webpack";
import { IWebpackConfigType } from "../type/webpack-config.type";
import { cssLoaderConfig } from "../../../loaders";

export const webpackModuleBuild = (options: IWebpackConfigType): RuleSetRule => {
    const cssLoader = cssLoaderConfig(options.isDev);
    const tsLoader = {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
    };

    const svgLoader = {
        test: /\.svg$/,
        use: ["@svgr/webpack"],
    };

    const fileLoader = {
        test: /\.(png|jpe?g|gif|)$/i,
        use: [
            {
                loader: "file-loader",
            },
        ],
    };

    return {
        rules: [
            cssLoader,
            fileLoader,
            tsLoader,
            svgLoader
        ],
    };
};