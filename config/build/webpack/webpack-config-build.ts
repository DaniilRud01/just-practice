import { Configuration } from "webpack";
import { IWebpackConfigType } from "./type/webpack-config.type";
import { webpackDevServerBuild } from "./webpack-dev-server-build/webpack-dev-server-build";
import { webpackModuleBuild } from "./webpack-module-build/webpack-module-build";
import { webpackPluginsBuild } from "./webpack-plugins-build/webpack-plugins-build";
import { webpackResolveBuild } from "./webpack-resolve-build/webpack-resolve-build";

export function webpackConfigBuild (options: IWebpackConfigType): Configuration {
    const { paths: { entry, output, html }, mode, isDev } = options;
    return {
        mode,
        module: webpackModuleBuild(options),
        entry,
        output: {
            filename: "[name].[contenthash].js",
            path: output,
            clean: true
        },
        plugins: webpackPluginsBuild(html, isDev),
        resolve: webpackResolveBuild(options),
        devtool: isDev ? "inline-source-map" : undefined,
        devServer: isDev ? webpackDevServerBuild(options) : undefined,
    };
}