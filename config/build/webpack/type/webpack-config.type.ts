
export type webpackMode = "production" | "development"

export interface IWebpackPaths {
    entry: string;
    output: string;
    html: string;
    src: string;
}

export interface IBuildEnv {
    mode: webpackMode,
    port: number,
}


export interface IWebpackConfigType {
    paths: IWebpackPaths;
    mode: webpackMode;
    isDev: boolean;
    port: number;
}