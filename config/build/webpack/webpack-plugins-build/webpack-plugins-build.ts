import htmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
import webpack, { WebpackPluginInstance } from "webpack";

export const webpackPluginsBuild = (paths: string, isDev: boolean): WebpackPluginInstance[] => {
    return [
        new htmlWebpackPlugin({
            template: paths
        }),
        new webpack.ProgressPlugin(),
        new MiniCssExtractPlugin({
            filename: "css/[name][contenthash:8].css",
            chunkFilename: "css/[name][contenthash:8].css"
        }),
        new BundleAnalyzerPlugin(),
        new webpack.DefinePlugin({
            __IS_DEV: isDev
        })
    ];
};