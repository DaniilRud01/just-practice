import MiniCssExtractPlugin from "mini-css-extract-plugin";

export const cssLoaderConfig = (isDev: boolean) => {
    return {
        test: /\.s[ac]ss$/i,
        use: [
            isDev ? "style-loader" : MiniCssExtractPlugin.loader,
            {
                loader: "css-loader",
                options: {
                    modules: {
                        localIdentName: "[path][name]__[local]--[hash:base64:5]",
                        auto: (path: string) => Boolean(path.includes(".module."))
                    }
                }
            },
            "sass-loader",
        ],
    };
};