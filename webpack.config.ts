import path from "path";
import webpack from "webpack";
import { IBuildEnv, IWebpackPaths, webpackMode } from "./config/build/webpack/type/webpack-config.type";
import { webpackConfigBuild } from "./config/build/webpack/webpack-config-build";

export default (env: IBuildEnv) => {
    const paths: IWebpackPaths = {
        entry: path.resolve(__dirname,"src", "index.tsx"),
        output: path.resolve(__dirname, "build"),
        html: path.resolve(__dirname, "public", "index.html"),
        src: path.resolve(__dirname, "src")
    };

    const mode: webpackMode = env.mode || "development";
    const PORT = env.port || 3000;

    const isDev = mode === "development";

    const config: webpack.Configuration = webpackConfigBuild({
        paths,
        mode,
        port: PORT,
        isDev
    });
    return config;
};