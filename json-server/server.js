/* eslint-disable */
const jsonServer = require("json-server");
const path = require("path");
const fs = require("fs");
const middlewares = jsonServer.defaults({});
const server = jsonServer.create();

const router = jsonServer.router(path.resolve(__dirname, "db.json"));

server.use(middlewares);
server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
    if (req.headers.authorization) {
        if (req.method === "POST") {
            req.body.createdAt = Date.now();
        }
        // Continue to JSON Server router
        next();
    } else {
        res.send({ "Error": "Unauthorized" });
    }
    
});
server.post("/login",  (req, res) => {
    const dbRead = JSON.parse(fs.readFileSync(path.resolve(__dirname, "db.json"), { encoding: "utf8" }));
    const findUser = dbRead.users.find((user) => user.name === req.body.name);

    if (findUser) {
        if (findUser.password === req.body.password) {
            res.json({ id: findUser.id, userName: findUser.name }).status(200)
        } else {
            res.status(401).send({ error: "Неверный пароль" })
        }
    } else {
        res.status(401).send("Пользователь не найден")
    }
});
// server.post("/login", (req, res) => {
//     const readDb = fs.readdirSync(path.resolve(__dirname, "db.json"), { encoding: "utf8" });
//     console.log(readDb);
//     res.send("OK");
// });
server.use(router);
server.listen(3001, () => {
    console.log("JSON Server is running PORT: 3001");
});