interface IUserSchema {
    authData?: IUser;
}

interface IUser {
    id?: number;
    userName?: string;
}

export type { IUserSchema, IUser };