import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IUser, IUserSchema } from "entities/user";

const initialState: IUserSchema = {
};
export const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        setAuthData: (state, action: PayloadAction<IUser>) => {
            state.authData = action.payload;
        },
        logOutUser: (state) => {
            state.authData = null;
            localStorage.removeItem("user");
        }
    }
});

export const { actions: userAction } = userSlice;
export const { reducer: userReducer } = userSlice;