import { createSlice } from "@reduxjs/toolkit";
import { IProfileSchema } from "../type/profile.type";

const initialState: IProfileSchema = {
    isLoading: false,
    readonly: false,
    error: null,
    profile: null
};
export const profileSlice = createSlice({
    name: "profile",
    initialState,
    reducers: {}
});

export const { actions: profileAction } = profileSlice;
export const { reducer: profileReducer } = profileSlice;