interface IProfile {
    name: string,
    surname: string;
    age: number;
    currency: string;
    country: string;
    city: string;
    userName: string;
    avatar: string;
}


interface IProfileSchema {
    profile?: IProfile;
    isLoading: boolean;
    error: string;
    readonly: boolean
}

export type {
    IProfileSchema,
};