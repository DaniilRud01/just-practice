import "@testing-library/jest-dom/extend-expect";

import { fireEvent, screen } from "@testing-library/react";
import { Counter } from "entities/counter/ui/counter";
import { RenderComponentWithRouting } from "app/lib";

describe("test counter component", () => {
    test("screen counter", () => {
        RenderComponentWithRouting(<Counter />, { counterState: { count: 4 } });
        expect(screen.getByTestId("counter-title")).toHaveTextContent("4");
    });

    test("counter increment", () => {
        RenderComponentWithRouting(<Counter />, { counterState: { count: 4 } });
        const title = screen.getByTestId("counter-title");
        const btn = screen.getByTestId("increment");
        fireEvent.click(btn);
        expect(title).toHaveTextContent("5");
    });

    test("counter decrement", () => {
        RenderComponentWithRouting(<Counter />, { counterState: { count: 4 } });
        const title = screen.getByTestId("counter-title");
        const btn = screen.getByTestId("decrement");
        fireEvent.click(btn);
        expect(title).toHaveTextContent("3");
    });
});