import React from "react";
import { Button } from "shared/ui";
import { useDispatch, useSelector } from "react-redux";
import { CounterAction, getCounterValue } from "entities/counter/model";
import { ButtonTheme } from "shared/ui/button/button";

export const Counter = () => {
    const dispatch = useDispatch();
    const value = useSelector(getCounterValue);

    const onPlusCount = () => {
        dispatch(CounterAction.increment());
    };

    const onMinusCount = () => {
        dispatch(CounterAction.decrement());
    };
    return (
        <div>
            <h1 data-testid={"counter-title"}>{value}</h1>
            <Button data-testid={"increment"} theme={ButtonTheme.BACKGROUND_INVERT} onClick={onPlusCount}>+</Button>
            <Button data-testid={"decrement"} theme={ButtonTheme.BACKGROUND_INVERT} onClick={onMinusCount}>-</Button>
        </div>
    );
};