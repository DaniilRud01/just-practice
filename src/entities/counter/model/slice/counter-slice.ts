import { createSlice } from "@reduxjs/toolkit";
import { ICounterSchema } from "entities/counter/model";

const initialState: ICounterSchema = {
    count: 0
};
export const counterSlice = createSlice({
    name: "counter",
    initialState,
    reducers: {
        increment: (state) => {
            state.count += 1;
        },
        decrement: (state) => {
            state.count -= 1;
        },
    }
});

export const { actions: CounterAction } = counterSlice;
export const { increment: CountPlus, decrement: CountMinus } = CounterAction;
export const { reducer: CounterReducer } = counterSlice;