import { CounterAction, CounterReducer, ICounterSchema } from "entities/counter/model";

describe("test counter slice", () => {
    test("test decrement reducer", () => {
        const state: ICounterSchema = { count: 1 };
        expect(CounterReducer(state, CounterAction.decrement)).toEqual({ count: 0 });
    });

    test("test increment reducer", () => {
        const state: ICounterSchema = { count: 1 };
        expect(CounterReducer(state, CounterAction.increment)).toEqual({ count: 2 });
    });

    test("should work with empty state", () => {
        expect(CounterReducer(undefined, CounterAction.increment)).toEqual({ count: 1 });
    });
});