import { createSelector } from "@reduxjs/toolkit";
import { getCounter } from "entities/counter/model/selectors/get-counter/get-counter";
import { ICounterSchema } from "entities/counter/model";

export const getCounterValue = createSelector(
    getCounter,
    (counter: ICounterSchema) => counter.count
);