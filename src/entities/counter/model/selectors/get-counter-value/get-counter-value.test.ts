import { IStateSchema } from "app/providers/store-provider";
import { DeepPartial } from "@reduxjs/toolkit";
import { getCounterValue } from "entities/counter/model";

describe("test get counter value", () => {
    test("should return counter value", () => {
        const state: DeepPartial<IStateSchema> = {
            counterState: {
                count: 4
            }
        };
        expect(getCounterValue(state as IStateSchema)).toEqual(4);
    });
});