import { IStateSchema } from "app/providers/store-provider";
import { DeepPartial } from "@reduxjs/toolkit";
import { getCounter } from "entities/counter/model";

describe("test counter", () => {
    test("should return counter", () => {
         const state: DeepPartial<IStateSchema> = {
             counterState: {
                 count: 4
             }
         };
         expect(getCounter(state as IStateSchema)).toEqual({ count: 4 });
    });
});