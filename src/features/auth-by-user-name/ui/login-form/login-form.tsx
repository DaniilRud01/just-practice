import styles from "./login-form.module.scss";

import React, { memo, useCallback } from "react";
import { Button, Text } from "shared/ui";
import { ButtonSize, ButtonTheme } from "shared/ui/button/button";
import { useTranslation } from "react-i18next";
import { Input } from "shared/ui/input/input";
import { classNames } from "app/lib";
import { useDispatch, useSelector } from "react-redux";
import { getLoginUserError, getLoginUserName, getLoginUserPassword, loginAction } from "features/auth-by-user-name";
import { loginUser } from "features/auth-by-user-name/model/service/login-user/login-user";
import { TEXT_SIZE, TEXT_THEME } from "shared/ui/text/text";

export const LoginForm = memo(() => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const userNameValue = useSelector(getLoginUserName);
    const userPasswordValue = useSelector(getLoginUserPassword);
    const userLoginError = useSelector(getLoginUserError);

    const onChangeUserName = useCallback((value: string) => {
        dispatch(loginAction.setUserName(value));
    }, [dispatch]);

    const onChangeUserPassword = useCallback((value: string) => {
        dispatch(loginAction.setUserPassword(value));
    }, [dispatch]);
    
    const getLoginUser = useCallback(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        dispatch(loginUser({ name: userNameValue, password: userPasswordValue }));
    }, [dispatch, userNameValue, userPasswordValue]);

    return (
        <div className={classNames(styles.wrapper)}>
            <Text size={TEXT_SIZE.L} theme={TEXT_THEME.PRIMARY} value={t("Авторизация")} />
            <Text size={TEXT_SIZE.S} theme={TEXT_THEME.ERROR} value={userLoginError} />
            <Input value={userNameValue} onChange={onChangeUserName} label={t("Введите имя")} />
            <Input value={userPasswordValue} onChange={onChangeUserPassword} label={t("Введите пароль")} />
            <Button onClick={getLoginUser} size={ButtonSize.S} className={classNames(styles.loginBtn)} theme={ButtonTheme.OUTLINE}>{t("Войти")}</Button>
        </div>
    );
});