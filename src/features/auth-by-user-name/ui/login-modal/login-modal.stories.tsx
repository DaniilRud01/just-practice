import { Meta, StoryObj } from "@storybook/react";
import { LoginModal } from "features/auth-by-user-name";
import { storeDecorator, translationDecorator } from "shared/config";


const meta:Meta<typeof LoginModal> = {
    component: LoginModal
};

export default meta;
type Story = StoryObj<typeof LoginModal>

export const Primary: Story = {
    args: {
        isOpen: true
    },
    decorators: [storeDecorator({ loginState: { userName: "scas", userPassword: "scsa" } }), translationDecorator()]
};