import React from "react";
import { Modal } from "shared/ui";
import { LoginForm } from "../login-form/login-form";

interface IProps {
    isOpen: boolean;
    onClose: () => void;
}
export const LoginModal = ({ isOpen, onClose }: IProps) => {
    return (
        <Modal
            lazy
            isOpen={isOpen}
            onClose={onClose}
        >
        <LoginForm />
        </Modal>
    );
};