import { getLoginState } from "../get-login-state/get-login-state";
import { ILoginSchema } from "features/auth-by-user-name";
import { createSelector } from "@reduxjs/toolkit";

export const getLoginUserPassword = createSelector(getLoginState, (loginState: ILoginSchema) => {
    return loginState.userPassword;
});