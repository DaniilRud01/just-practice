import { createSelector } from "@reduxjs/toolkit";
import { getLoginState } from "../get-login-state/get-login-state";
import { ILoginSchema } from "features/auth-by-user-name";

export const getLoginUserError = createSelector(getLoginState, (state: ILoginSchema) => {
    return state.error;
});