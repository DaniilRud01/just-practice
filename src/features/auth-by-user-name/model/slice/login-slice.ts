import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ILoginSchema } from "../types/login-schema";
import { loginUser } from "features/auth-by-user-name/model/service/login-user/login-user";

const initialState: ILoginSchema = {
    userName: "",
    userPassword: "",
    isLoading: false,
    error: null,
};
export const loginSlice = createSlice({
    name: "login",
    initialState,
    reducers: {
        setUserName: (state, action: PayloadAction<string>) => {
            state.userName = action.payload;
        },
        setUserPassword: (state, action: PayloadAction<string>) => {
            state.userPassword = action.payload;
        },
    },
    extraReducers: (builder) => {
            builder.addCase(loginUser.pending, (state, action) => {
                state.error = null;
                state.isLoading = true;
            });
        builder.addCase(loginUser.fulfilled, (state, action) => {
            localStorage.setItem("user", JSON.stringify(action.payload));
            state.isLoading = false;
        });
        builder.addCase(loginUser.rejected, (state, action) => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            state.error = action.payload;
            state.isLoading = false;
        });
    }
});

export const { actions: loginAction } = loginSlice;
export const { reducer: loginReducer } = loginSlice;