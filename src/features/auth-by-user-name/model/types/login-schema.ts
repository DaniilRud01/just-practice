interface ILoginSchema {
    userName?: string;
    userPassword?: string;
    isLoading?: boolean;
    error?: string;
}

export type {
    ILoginSchema
};