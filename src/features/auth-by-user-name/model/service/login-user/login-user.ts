import { createAsyncThunk } from "@reduxjs/toolkit";
import { IUserSchema, userAction } from "entities/user";
import axios from "axios";
import i18n from "i18next";

interface ILoginDto {
    name: string,
    password: string;
}

export const loginUser = createAsyncThunk<IUserSchema, ILoginDto>(
    "user/login",
    async (authData: ILoginDto, thunkAPI) => {
        try {
            const response = await axios.post<IUserSchema>("http://localhost:3001/login", authData, { headers: { "authorization": "12" } });
            if (!response.data) {
                throw new Error("Error");
            }
            thunkAPI.dispatch(userAction.setAuthData(response.data));
            return response.data;
        } catch (e) {
           return thunkAPI.rejectWithValue(i18n.t("Неверный логин или пароль"));
        }
    }
);
