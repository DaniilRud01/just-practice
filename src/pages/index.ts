export { AboutPageAsync as AboutPage } from "./about/ui/about.async";
export { MainPageAsync as MainPage } from "./main/ui/main.async";
export { ProfilePageAsync as ProfilePage } from "./profile/ui/profile.async";
export { NotFoundPage } from "./not-found";