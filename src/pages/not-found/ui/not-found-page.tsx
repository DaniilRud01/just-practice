import styles from "./not-found-page.module.scss";

import React from "react";
import { classNames } from "app/lib";
import { useTranslation } from "react-i18next";

export const NotFoundPage = () => {
    const { t } = useTranslation();
    return (
        <div className={classNames(styles.notFoundPage)}>
            {
                t("Страница не найдена")
            } 
        </div>
    );
};