import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { NotFoundPage } from "pages";
import { ThemeVariant } from "app/enums";


const meta:Meta<typeof NotFoundPage> = {
    component: NotFoundPage
};

export default meta;
type Story = StoryObj<typeof NotFoundPage>
export const NavbarDark: Story = {
};

export const NavbarLight: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};