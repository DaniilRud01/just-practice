import React from "react";
import { useTranslation } from "react-i18next";
import { Counter } from "entities/counter/ui/counter";

const Main = () => {
    const { t } = useTranslation("main");
    return (
        <div>
            {t("Главная")}
            <Counter />
        </div>
    );
};

export default Main;