import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { MainPage } from "pages";
import { ThemeVariant } from "app/enums";


const meta:Meta<typeof MainPage> = {
    component: MainPage
};

export default meta;
type Story = StoryObj<typeof MainPage>
export const NavbarDark: Story = {
};

export const NavbarLight: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};