import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { ThemeVariant } from "app/enums";
import { ProfilePage } from "pages";


const meta:Meta<typeof ProfilePage> = {
    component: ProfilePage
};

export default meta;
type Story = StoryObj<typeof ProfilePage>
export const NavbarDark: Story = {
};

export const NavbarLight: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};