import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { AboutPage } from "pages";
import { ThemeVariant } from "app/enums";


const meta:Meta<typeof AboutPage> = {
    component: AboutPage
};

export default meta;
type Story = StoryObj<typeof AboutPage>
export const NavbarDark: Story = {
};

export const NavbarLight: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};