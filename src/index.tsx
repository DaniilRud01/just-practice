import "app/styles/index.scss";

import { BrowserRouter } from "react-router-dom";
import { createRoot } from "react-dom/client";
import { App } from "app/app";
import { ThemeProvider } from "app/providers";
import ErrorBoundary from "app/providers/error-boundary/ui/error-boundary";
import { StoreProvider } from "app/providers/store-provider";

const container = document.getElementById("root");
const root = createRoot(container);
const app =
    <StoreProvider>
        <BrowserRouter>
            <ErrorBoundary>
                <ThemeProvider>
                    <App/>
                </ThemeProvider>
            </ErrorBoundary>
        </BrowserRouter>
    </StoreProvider>;

root.render(app);
