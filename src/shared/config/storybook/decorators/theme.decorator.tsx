import { ThemeVariant } from "app/enums";
import { Story } from "@storybook/react";

export const themeDecorator = (theme: ThemeVariant) => (StoryComponent: Story) => {
    return (
        <div className={`app ${theme}`}>
            <StoryComponent />
        </div>
    );
};