import { IStateSchema, StoreProvider } from "app/providers/store-provider";
import { Story } from "@storybook/react";

export const storeDecorator = (store: IStateSchema) => (StoryComponent: Story) => {
    return <StoreProvider initialState={store}>
        <StoryComponent />
    </StoreProvider>;
};