import { AboutPage, MainPage, NotFoundPage, ProfilePage } from "pages";
import { RouteProps } from "react-router-dom";

export enum appRoute {
    MAIN = "main",
    ABOUT = "about",
    PROFILE = "profile",
    NOT_FOUND = "*"
}

export const appRoutePaths: Record<appRoute, string> = {
    [appRoute.MAIN]: "/",
    [appRoute.ABOUT]: "/about",
    [appRoute.PROFILE]: "/profile",
    [appRoute.NOT_FOUND]: "*"
};

export const appRouteConfig: Record<appRoute, RouteProps> = {
    [appRoute.MAIN]: {
        path: appRoutePaths.main,
        element: <MainPage />
    },
    [appRoute.ABOUT]: {
        path: appRoutePaths.about,
        element: <AboutPage />
    },
    [appRoute.PROFILE]: {
        path: appRoutePaths.profile,
        element: <ProfilePage />
    },
    [appRoute.NOT_FOUND]: {
        path: appRoutePaths["*"],
        element: <NotFoundPage />
    }
};