import { Meta, StoryObj } from "@storybook/react";
import { Button } from "shared/ui";
import { ButtonSize, ButtonTheme } from "shared/ui/button/button";
import { themeDecorator } from "shared/config/storybook";
import { ThemeVariant } from "app/enums";


const meta:Meta<typeof Button> = {
    component: Button
};

export default meta;
type Story = StoryObj<typeof Button>

export const Default: Story = {
    args: {
        children: "Default Button"
    }
};

export const Clear: Story = {
    args: {
        theme: ButtonTheme.CLEAR,
        children: "Clear Button"
    }
};

export const OutlineLight: Story = {
    args: {
        theme: ButtonTheme.OUTLINE,
        children: "Outline Button",
    },
    decorators: [themeDecorator(ThemeVariant.LIGHT)]
};

export const OutlineDark: Story = {
    args: {
        theme: ButtonTheme.OUTLINE,
        children: "Outline Button",
    },
    decorators: [themeDecorator(ThemeVariant.DARK)]
};

export const Background: Story = {
    args: {
        theme: ButtonTheme.BACKGROUND,
        children: "Outline Button",
    },
    decorators: [themeDecorator(ThemeVariant.DARK)]
};

export const BackgroundInvert: Story = {
    args: {
        theme: ButtonTheme.BACKGROUND_INVERT,
        children: "Outline Button",
    },
    decorators: [themeDecorator(ThemeVariant.DARK)]
};

export const SizeS: Story = {
    args: {
        theme: ButtonTheme.BACKGROUND_INVERT,
        children: "Outline Button",
        size: ButtonSize.S
    },
    decorators: [themeDecorator(ThemeVariant.DARK)]
};

export const SizeL: Story = {
    args: {
        theme: ButtonTheme.BACKGROUND_INVERT,
        children: "Outline Button",
        size: ButtonSize.L
    },
    decorators: [themeDecorator(ThemeVariant.DARK)]
};

export const SizeXl: Story = {
    args: {
        theme: ButtonTheme.BACKGROUND_INVERT,
        children: "Outline Button",
        size: ButtonSize.XL
    },
    decorators: [themeDecorator(ThemeVariant.DARK)]
};