import styles from "./button.module.scss";

import { classNames } from "app/lib";
import React, { ButtonHTMLAttributes, PropsWithChildren } from "react";

export enum ButtonTheme {
    CLEAR = "clear",
    OUTLINE = "outline",
    BACKGROUND = "background",
    BACKGROUND_INVERT = "backgroundInvert"
}

export enum ButtonSize {
    S = "small",
    L = "large",
    XL = "extraLarge"
}

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement>{
    theme?: ButtonTheme
    size?: ButtonSize
}

export const Button = ({ className, children, theme, size = ButtonSize.L, ...props }: PropsWithChildren<IButtonProps>) => {
    const additionalClasses = [
        styles[theme],
        styles[size]
    ];

    return (
        <button {...props}
                className={
            classNames(styles.button, {}, [className, ...additionalClasses])
        }
        >
            {children}
        </button>
    );
};