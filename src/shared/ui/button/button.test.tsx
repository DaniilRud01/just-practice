import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Button, ButtonTheme } from "./button";

describe("button component", () => {
    test("screen button", () => {
        render(<Button>Test</Button>);
        expect(screen.getByText("Test")).toBeInTheDocument();
    });

    test("theme button", () => {
        render(<Button theme={ButtonTheme.CLEAR}>Test</Button>);
        expect(screen.getByText("Test")).toHaveClass("clear");
    });
});