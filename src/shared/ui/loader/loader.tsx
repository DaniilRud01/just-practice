import styles from "./loader.module.scss";

import React from "react";
import { classNames } from "app/lib";

export const Loader = () => {
    return <span className={classNames(styles.loader)} />;
};