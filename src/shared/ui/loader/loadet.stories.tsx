import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { ThemeVariant } from "app/enums";
import { Loader } from "shared/ui";


const meta:Meta<typeof Loader> = {
    component: Loader
};

export default meta;
type Story = StoryObj<typeof Loader>
export const LoaderDark: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};

export const LoaderLight: Story = {
};