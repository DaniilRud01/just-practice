import { Meta, StoryObj } from "@storybook/react";
import { Text } from "shared/ui";
import { TEXT_SIZE, TEXT_THEME } from "shared/ui/text/text";


const meta:Meta<typeof Text> = {
    component: Text
};

export default meta;
type Story = StoryObj<typeof Text>

export const PrimarySmall: Story = {
    args: {
        value: "Primary Text small size",
        theme: TEXT_THEME.PRIMARY,
        size: TEXT_SIZE.S
    }
};

export const PrimaryMedium: Story = {
    args: {
        value: "Primary Text medium size",
        theme: TEXT_THEME.PRIMARY,
        size: TEXT_SIZE.L
    }
};

export const PrimaryLarge: Story = {
    args: {
        value: "Primary Text large size",
        theme: TEXT_THEME.PRIMARY,
        size: TEXT_SIZE.XL
    }
};

export const ErrorSmall: Story = {
    args: {
        value: "Error Text small size",
        theme: TEXT_THEME.ERROR,
        size: TEXT_SIZE.S
    }
};

export const ErrorMedium: Story = {
    args: {
        value: "Error Text medium size",
        theme: TEXT_THEME.ERROR,
        size: TEXT_SIZE.L
    }
};

export const ErrorLarge: Story = {
    args: {
        value: "Error Text large size",
        theme: TEXT_THEME.ERROR,
        size: TEXT_SIZE.XL
    }
};