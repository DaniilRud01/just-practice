import styles from "./text.module.scss";

import React from "react";
import { classNames } from "app/lib";

export enum TEXT_SIZE {
    S = "size_s",
    L = "size_l",
    XL = "size_xl"
}

export enum TEXT_THEME {
    PRIMARY = "primary",
    ERROR = "error"
}
interface IProps {
    size: TEXT_SIZE,
    theme: TEXT_THEME,
    value: string
}
export const Text = ({ size, theme, value }: IProps) => {
    return <span className={classNames(`${styles.text} ${styles[size]} ${styles[theme]}`)}>{value}</span>;
};