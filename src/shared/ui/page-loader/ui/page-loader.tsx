import styles from "./page-loader.module.scss";

import React from "react";
import { classNames } from "app/lib";
import { Loader } from "shared/ui";

export const PageLoader = () => {
    return (
        <div className={classNames(styles.pageLoader)}>
            <Loader />
        </div>
    );
};