import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { ThemeVariant } from "app/enums";
import { PageLoader } from "shared/ui/page-loader";


const meta:Meta<typeof PageLoader> = {
    component: PageLoader
};

export default meta;
type Story = StoryObj<typeof PageLoader>
export const PageLoaderDark: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};

export const PageLoaderLight: Story = {
};