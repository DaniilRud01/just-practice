import styles from "./input.module.scss";

import React, { ChangeEvent, InputHTMLAttributes, memo } from "react";
import { classNames } from "app/lib";

interface IProps extends Omit<InputHTMLAttributes<HTMLInputElement>, "onChange">{
    className?: string;
    onChange?: (value: string) => void;
    label?: string
    type?: string;
    value?: string;
}

export const Input = memo(({ className, onChange, label, type = "text", value, ...otherProps }: IProps) => {
    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        onChange?.(event.target.value);
    };
    
    return (
        <div className={classNames(styles.wrapper)}>
            <span className={classNames(styles.label)}>{label}</span>
            <input
                value={value}
                onChange={handleChange} 
                type={type} 
                className={classNames(styles.input, {}, [className])}
                {...otherProps}
            />
        </div>
    );
});