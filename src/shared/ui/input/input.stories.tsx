import { Meta, StoryObj } from "@storybook/react";
import { Input } from "shared/ui/input/input";


const meta:Meta<typeof Input> = {
    component: Input
};

export default meta;
type Story = StoryObj<typeof Input>

export const Primary: Story = {
    args: {
        label: "Primary Input",
    }
};