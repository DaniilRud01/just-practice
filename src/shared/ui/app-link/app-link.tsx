import styles from "./app-link.module.scss";

import { classNames } from "app/lib";
import React, { PropsWithChildren } from "react";
import { Link, LinkProps } from "react-router-dom";
type IAppLink = LinkProps

export const AppLink = ({ children, to, className, ...props }: PropsWithChildren<IAppLink>) => {
    return (
        <Link className={classNames(styles.link, {}, [className])} {...props} to={to}>
            {children}
        </Link>
    );
};