import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { AppLink } from "shared/ui";
import { ThemeVariant } from "app/enums";


const meta:Meta<typeof AppLink> = {
    component: AppLink,
    args: {
        to: "/"
    }
};

export default meta;
type Story = StoryObj<typeof AppLink>

export const Primary: Story = {
    args: {
        children: "Link"
    }
};


export const PrimaryDark: Story = {
    args: {
        children: "Link"
    },
    decorators: [themeDecorator(ThemeVariant.DARK)]
};

