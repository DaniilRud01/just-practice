import styles from "./modal.module.scss";

import React, { lazy, memo, MouseEvent, ReactNode, useCallback, useEffect, useRef, useState } from "react";
import { classNames } from "app/lib";

interface IModal {
    children?: ReactNode;
    className?: string;
    isOpen: boolean;
    onClose: () => void;
    lazy?: boolean;
}

const TIMEOUT_DELAY = 200;
export const Modal = memo(({ children, className, onClose, isOpen }: IModal) => {
    const [isCloseModal, setIsCloseModal] = useState<boolean>(false);
    const [isMounted, setIsMounted] = useState<boolean>(false);
    const timeOutRef = useRef<ReturnType<typeof setTimeout>>();
    const mods: Record<string, boolean> = {
        [styles.openModal]: isOpen,
        [styles.closeModal]: isCloseModal,
    };

    const handleClose = useCallback(() => {
        if (onClose) {
            setIsCloseModal(true);
            timeOutRef.current = setTimeout(() => {
                setIsCloseModal(false);
                onClose();
            }, TIMEOUT_DELAY);
        }
    }, [onClose]);

    const onClickContent = useCallback((e: MouseEvent) => {
        e.stopPropagation();
    }, []);

    useEffect(() => {
        if (isOpen) {
            setIsMounted(true);
        }
        return () => {
            clearTimeout(timeOutRef.current);
        };
    }, [isOpen]);

    if (lazy && !isMounted) {
        return null;
    }

    return (
            <div className={classNames(styles.wrapper, mods, [className])}>
                <div onClick={handleClose} className={classNames(styles.overlay)}>
                    <div onClick={onClickContent} className={classNames(styles.content)}>
                        {children}
                    </div>
                </div>
            </div>
    );
});