import { Meta, StoryObj } from "@storybook/react";
import { Modal } from "shared/ui";

const meta:Meta<typeof Modal> = {
    component: Modal
};

export default meta;
type Story = StoryObj<typeof Modal>

export const ModalOpen: Story = {
    args: {
        children: "Default text",
        isOpen: true
    }
};