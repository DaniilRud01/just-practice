import { classNames, useTheme } from "app/lib";
import { AppRoute } from "app/providers/router";
import React, { Suspense } from "react";
import { Navbar, Sidebar } from "widgets";

import "../shared/config/i18n/i18n";

export const App = (): JSX.Element => {
    const { theme } = useTheme();
    return (
        <Suspense fallback="">
        <div className={classNames("app", { open: true, close: true }, [theme])}>
            <Navbar/>
            <div className={"app-content"}>
                <Sidebar/>
                <AppRoute/>
            </div>
        </div>
        </Suspense>
    );
};