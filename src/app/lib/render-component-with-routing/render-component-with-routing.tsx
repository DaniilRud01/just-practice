import React, { ReactNode } from "react";
import { IStateSchema, StoreProvider } from "app/providers/store-provider";
import { DeepPartial } from "@reduxjs/toolkit";
import { render } from "@testing-library/react";


export const RenderComponentWithRouting = (component: ReactNode, initialState: DeepPartial<IStateSchema> = {}) => {
    return render(
        <StoreProvider initialState={initialState as IStateSchema}>
                {component}
        </StoreProvider>
    );
};
