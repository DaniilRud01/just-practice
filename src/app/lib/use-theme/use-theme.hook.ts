import { THEME_LOCAL_STORAGE_KEY, ThemeContext } from "app/providers";
import { useContext } from "react";
import { ThemeVariant } from "app/enums/theme.enum";

interface IUseTheme {
    theme: ThemeVariant,
    handleChangeTheme: () => void;
}

export const useTheme = (): IUseTheme => {
   const { theme, onChangeTheme } = useContext(ThemeContext);

    const handleChangeTheme = () => {
        const newTheme = theme === ThemeVariant.LIGHT ? ThemeVariant.DARK : ThemeVariant.LIGHT;
        onChangeTheme(newTheme);
        localStorage.setItem(THEME_LOCAL_STORAGE_KEY, newTheme);
    };

    return {
        theme,
        handleChangeTheme
    };
};