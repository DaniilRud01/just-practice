
type modsType = Record<string, string | boolean>

export const classNames = (className: string, mods: modsType = {}, additionalCls: string[] = []): string => {
    return [
        className,
        ...additionalCls,
        ...Object.entries(mods).filter(([_, value]) => Boolean(value)).map(([className, _]) => className)
    ].join(" ");
};