import { classNames } from "app/lib";

describe("class names", () => {
    test("check one class", () => {
        expect(classNames("class")).toBe("class");
    });

    test("check additional class", () => {
        expect(classNames("class", {}, ["additional"])).toBe("class additional");
    });

    test("check mods true class", () => {
        expect(classNames("class", { hover: true }, ["additional"])).toBe("class additional hover");
    });

    test("check mods true with false class", () => {
        expect(classNames("class", { hover: true, hidden: false }, ["additional"])).toBe("class additional hover");
    });

    test("check mods undefined with false class", () => {
        expect(classNames("class", { hover: true, hidden: undefined }, ["additional"])).toBe("class additional hover");
    });
});