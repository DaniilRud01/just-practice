import React, { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import { appRoutes } from "shared/config";
import { PageLoader } from "shared/ui/page-loader";

export const AppRoute = () => {
    return (
            <Routes>
                {
                    Object.values(appRoutes).map((props) => (
                        <Route key={props.path} path={props.path} element={
                            <Suspense fallback={<PageLoader />}>
                            <div className={"content-wrapper"}>
                                {props.element}
                            </div>
                            </Suspense>
                        } />
                    ))
                }
            </Routes>
    );
};
