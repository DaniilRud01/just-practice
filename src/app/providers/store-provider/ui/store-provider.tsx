import React, { ReactNode } from "react";
import { Provider } from "react-redux";
import { createReduxStore, IStateSchema } from "app/providers/store-provider";


interface IStoreProvider {
    children?: ReactNode;
    initialState?: IStateSchema;
}
export const StoreProvider = ({ children, initialState }: IStoreProvider) => {
    const store = createReduxStore(initialState);
    return (
        <Provider store={store}>
            {children}
        </Provider>
    );
};