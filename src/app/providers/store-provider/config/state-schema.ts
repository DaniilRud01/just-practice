import { ICounterSchema } from "entities/counter/model";
import { IUserSchema } from "entities/user";
import { ILoginSchema } from "features/auth-by-user-name";
import { IProfileSchema } from "entities/profile";

interface IStateSchema {
    counterState?: ICounterSchema
    user?: IUserSchema
    loginState?: ILoginSchema
    profile?: IProfileSchema
}

export type {
    IStateSchema
};
