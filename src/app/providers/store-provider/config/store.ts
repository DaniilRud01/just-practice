import { configureStore, ReducersMapObject } from "@reduxjs/toolkit";
import { IStateSchema } from "app/providers/store-provider";
import { CounterReducer } from "entities/counter/model";
import { userReducer } from "entities/user/model/slice/user-slice";
import { loginReducer } from "features/auth-by-user-name";
import { profileReducer } from "entities/profile";


export const createReduxStore = (initialState?: IStateSchema) => {
    const rootReducer: ReducersMapObject<IStateSchema> = {
        counterState: CounterReducer,
        user: userReducer,
        loginState: loginReducer,
        profile: profileReducer
    };
    return configureStore<IStateSchema>({
        reducer: rootReducer,
        devTools: __IS_DEV,
        preloadedState: initialState
    });
};