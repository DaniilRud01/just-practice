import { ThemeVariant } from "app/enums";
import { createContext } from "react";

interface IThemeContext {
    theme?: ThemeVariant
    onChangeTheme?: (theme: ThemeVariant) => void;
}

export const ThemeContext = createContext<IThemeContext>({});

export const THEME_LOCAL_STORAGE_KEY = "theme";
