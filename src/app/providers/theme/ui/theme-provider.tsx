import { ThemeVariant } from "app/enums";
import React, { ReactNode, useMemo, useState } from "react";
import { THEME_LOCAL_STORAGE_KEY, ThemeContext } from "app/providers";

interface IThemeProvider {
    children: ReactNode
}


export const ThemeProvider = ({ children }: IThemeProvider) => {
    const defaultTheme = localStorage.getItem(THEME_LOCAL_STORAGE_KEY) as ThemeVariant || ThemeVariant.LIGHT;
    const [themeVariant, setThemeVariant] = useState<ThemeVariant>(defaultTheme);

    const defaultValue = useMemo(() => {
        return {
            theme: themeVariant,
            onChangeTheme: setThemeVariant
        };
    }, [themeVariant]);

    return (
        <ThemeContext.Provider value={defaultValue}>
            {children}
        </ThemeContext.Provider>
    );
};