import styles from "./sidebar.module.scss";
import { LanguageSwitcher, ThemeSwitcher } from "widgets";

import { classNames } from "app/lib";
import React, { useState } from "react";
import { Button } from "shared/ui";
import { ButtonSize, ButtonTheme } from "shared/ui/button/button";

export const Sidebar = () => {
    const [isOpen, setIsOpen] = useState<boolean>(false);

    const handleOpen = () => {
        setIsOpen((prev) => !prev);
    };
    return (
        <div data-testid="sidebar" className={classNames(styles.sidebar, { [styles.close]: isOpen })}>
            <Button
                size={ButtonSize.L}
                theme={ButtonTheme.BACKGROUND_INVERT}
                className={classNames(styles.collapseBtn)}
                data-testid={"toggle"}
                onClick={handleOpen}>
                { isOpen ? ">" : "<" }
            </Button>
            <div className={classNames(styles.switchers)}>
                <ThemeSwitcher />
                <LanguageSwitcher className={classNames(styles.lang)} />
            </div>
        </div>
    );
};