import { fireEvent, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Sidebar } from "widgets";
import { testComponentWithTranslation } from "app/lib/test-component-with-translation/test-component-with-translation";

describe("Sidebar component", () => {
    test("screen sidebar", () => {
        testComponentWithTranslation(<Sidebar />);
        expect(screen.getByTestId("sidebar")).toBeInTheDocument();
    });

    test("close sidebar", () => {
        testComponentWithTranslation(<Sidebar />);
        expect(screen.getByTestId("sidebar")).toBeInTheDocument();
        const toggleBtn = screen.getByTestId("toggle");
        expect(screen.getByTestId("toggle")).toBeInTheDocument();
        fireEvent.click(toggleBtn);
        expect(screen.getByTestId("sidebar")).toHaveClass("close");
    });
});