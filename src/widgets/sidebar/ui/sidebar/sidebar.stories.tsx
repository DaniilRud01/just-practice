import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { ThemeVariant } from "app/enums";
import { Sidebar } from "widgets";


const meta:Meta<typeof Sidebar> = {
    component: Sidebar
};

export default meta;
type Story = StoryObj<typeof Sidebar>
export const SidebarLight: Story = {
};

export const SidebarDark: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};