import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { ThemeVariant } from "app/enums";
import { PageError } from "widgets/page-error";


const meta:Meta<typeof PageError> = {
    component: PageError
};

export default meta;
type Story = StoryObj<typeof PageError>
export const NavbarLight: Story = {
};

export const NavbarDark: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};