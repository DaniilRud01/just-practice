import React from "react";
import { useTranslation } from "react-i18next";
import { Button } from "shared/ui";

export const PageError = () => {
    const { t } = useTranslation();

    const onReloadPage = () => location.reload();
    return (
        <div>
            <h1>{t("Произошла ошибка")}</h1>
            <Button onClick={onReloadPage}>{t("Перезагрузить страницу")}</Button>
        </div>
    );
};