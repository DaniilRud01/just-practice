import React from "react";
import DarkThemeIcon from "shared/assets/icons/theme-dark.svg";
import LightThemeIcon from "shared/assets/icons/theme-light.svg";
import { ThemeVariant } from "app/enums";
import { useTheme } from "app/lib";
import { Button } from "shared/ui";
import { ButtonTheme } from "shared/ui/button/button";

export const ThemeSwitcher = () => {
    const { handleChangeTheme, theme } = useTheme();
    const isDarkTheme = theme === ThemeVariant.DARK;
    return (
        <Button onClick={handleChangeTheme} theme={ButtonTheme.CLEAR}>
            {
                isDarkTheme ? <DarkThemeIcon/> : <LightThemeIcon/>
            }
        </Button>
    );
};