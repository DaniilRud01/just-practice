import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { ThemeVariant } from "app/enums";
import { ThemeSwitcher } from "widgets";


const meta:Meta<typeof ThemeSwitcher> = {
    component: ThemeSwitcher
};

export default meta;
type Story = StoryObj<typeof ThemeSwitcher>
export const ThemeSwitcherLight: Story = {
};

export const ThemeSwitcherDark: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};