import { AppLink, Button } from "shared/ui";
import styles from "./navbar.module.scss";

import { classNames } from "app/lib";
import React, { memo, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { ButtonTheme } from "shared/ui/button/button";
import { LoginModal } from "features/auth-by-user-name";
import { useDispatch, useSelector } from "react-redux";
import { getUerAuthData, userAction } from "entities/user";

export const Navbar = memo(() => {
    const [isOpenModal, setIsOpenModal] = useState(false);
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const isHasUser: string | null = useSelector(getUerAuthData);

    const onLoginOrOnLogoutUser = useCallback(() => {
        if (isHasUser) {
            dispatch(userAction.logOutUser());
            setIsOpenModal(false);
        } else {
            console.log("test open modal");
            setIsOpenModal(true);
        }
    }, [dispatch, isHasUser]);

    return (
        <div className={classNames(styles.navBar)}>
            <AppLink className={classNames(styles.link)} to={"/"}>{t("Главная")}</AppLink>
            <AppLink className={classNames(styles.link)} to={"/about"}>{t("О нас")}</AppLink>
            <AppLink className={classNames(styles.link)} to={"/profile"}>{t("Профиль")}</AppLink>
            <Button theme={ButtonTheme.CLEAR}
                    onClick={onLoginOrOnLogoutUser}>{isHasUser ? t("Выйти") : t("Войти")}</Button>
            {!isHasUser && <LoginModal isOpen={isOpenModal} onClose={() => setIsOpenModal(false)}/>}
        </div>
    );
});