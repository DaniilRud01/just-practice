import { Meta, StoryObj } from "@storybook/react";
import { themeDecorator } from "shared/config/storybook";
import { ThemeVariant } from "app/enums";
import { Navbar } from "widgets";


const meta:Meta<typeof Navbar> = {
    component: Navbar
};

export default meta;
type Story = StoryObj<typeof Navbar>
export const NavbarDark: Story = {
};

export const NavbarLight: Story = {
    decorators: [themeDecorator(ThemeVariant.DARK)]
};