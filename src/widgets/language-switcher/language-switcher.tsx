import React, { ButtonHTMLAttributes } from "react";
import { useTranslation } from "react-i18next";
import { Button } from "shared/ui";
import { ButtonTheme } from "shared/ui/button/button";

type IProps = ButtonHTMLAttributes<HTMLButtonElement>

export const LanguageSwitcher = ({ ...props }: IProps) => {
    const { t, i18n } = useTranslation();

    const toggle = () => i18n.changeLanguage(i18n.language === "ru" ? "en" : "ru");
    return (
        <Button {...props} theme={ButtonTheme.CLEAR} onClick={toggle}>{t("Язык")}</Button>
    );
};